//
//  VenueTableViewCell.swift
//  MerlinTest
//
//  Created by Sergio Bravo on 6/12/19.
//  Copyright © 2019 Sergio Bravo. All rights reserved.
//

import UIKit
import Kingfisher

class VenueTableViewCell: UITableViewCell {
    @IBOutlet weak var venueImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var currentPeopleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(venue: MerlinVenue) {
        selectionStyle = .none
        nameLabel.text = venue.name ?? ""
        currentPeopleLabel.text = "\(venue.currentPeopleCount ?? 0)"
        
        if let category = venue.categories?.first {
            categoryLabel.text = category.name
        }
        
        guard let url = venue.categories?.first?.imageURL(size: .big) else {
            return
        }
        venueImageView.kf.setImage(with: url)
    }

}
