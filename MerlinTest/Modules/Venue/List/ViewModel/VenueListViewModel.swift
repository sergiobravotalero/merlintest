//
//  VenueListViewModel.swift
//  MerlinTest
//
//  Created by Sergio Bravo on 6/12/19.
//  Copyright © 2019 Sergio Bravo. All rights reserved.
//

import Foundation

class VenueListViewModel {
    var venues: [MerlinVenue] = []
    
    func fetchValues(completion: @escaping(_ : Bool) -> Void) {
        APIManager().getGroups { [weak self] groups in
            guard let items = groups?.groups?.first?.items else {
                completion(false)
                return
            }
            for item in items {
                guard let venue = item.venue else { continue }
                self?.venues.append(venue)
            }
            completion(true)
        }
    }
}
