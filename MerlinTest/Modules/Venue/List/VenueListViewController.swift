//
//  VenueListViewController.swift
//  MerlinTest
//
//  Created by Sergio Bravo on 6/12/19.
//  Copyright © 2019 Sergio Bravo. All rights reserved.
//

import UIKit

class VenueListViewController: UIViewController {
    // MARK: - Properties
    @IBOutlet weak var venueImageView: UIImageView!
    var viewModel: VenueListViewModel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        viewModel = VenueListViewModel()
        viewModel.fetchValues { [weak self] success in
            guard success else { return }
            self?.tableView.reloadData()
        }
    }
    // MARK: - IBActions
    @IBAction func userDidTapShowVenues(_ sender: Any) {
        guard let mapVC = UIStoryboard(name: "VenuesMap", bundle: nil).instantiateInitialViewController() as? VenueMapViewController else {
                return
        }
        navigationController?.pushViewController(mapVC, animated: true)
        
    }
}

extension VenueListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.venues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let venue = viewModel.venues[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "VenueTableViewCell", for: indexPath) as! VenueTableViewCell
        cell.configure(venue: venue)
        return cell
    }
}
