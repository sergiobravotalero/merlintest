//
//  MerlinItem.swift
//  MerlinTest
//
//  Created by Sergio Bravo on 6/12/19.
//  Copyright © 2019 Sergio Bravo. All rights reserved.
//

import Foundation
import ObjectMapper

class MerlinGroups: Mappable {
    var groups: [MerlinGroup]?
    required init?(map: Map){}
    func mapping(map: Map) {
        groups <- map["groups"]
    }
}

class MerlinGroup: Mappable {
    var type: String?
    var name: String?
    var items: [MerlinItem]?
    required init?(map: Map){}
    func mapping(map: Map) {
        type <- map["type"]
        name <- map["name"]
        items <- map["items"]
    }
}


class MerlinItem: Mappable {
    var venue: MerlinVenue?
    required init?(map: Map){}
    func mapping(map: Map) {
        venue <- map["venue"]
    }
}

class MerlinVenue: Mappable {
    var id: String?
    var name: String?
    var location: VenueLocation?
    var currentPeopleCount: Int?
    var categories: [VenueCategory]?
    required init?(map: Map){}
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        location <- map["location"]
        currentPeopleCount <- map["hereNow.count"]
        categories <- map["categories"]
    }
}

class VenueLocation: Mappable {
    var latitude: Double?
    var longitude: Double?
    required init?(map: Map){}
    func mapping(map: Map) {
        latitude <- map["lat"]
        longitude <- map["lng"]
    }
}

enum ImageSizes: Int {
    case small = 32
    case medium = 64
    case big = 100
}

class VenueCategory: Mappable {
    var name: String?
    var iconPrefix: String?
    var iconSuffix: String?
    required init?(map: Map){}
    func mapping(map: Map) {
        name <- map["name"]
        iconPrefix <- map["icon.prefix"]
        iconSuffix <- map["icon.suffix"]
    }
    func imageURL(size: ImageSizes) -> URL? {
        guard let prefix = iconPrefix, let suffix = iconSuffix else {
            return nil
        }
        let urlString = "\(prefix)bg_\(size.rawValue)\(suffix)"
        return URL(string: urlString)
    }
}
