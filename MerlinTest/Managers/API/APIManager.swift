//
//  APIManager.swift
//  MerlinTest
//
//  Created by Sergio Bravo on 6/12/19.
//  Copyright © 2019 Sergio Bravo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

//enum Services: String {
//    case clientID = "THIZC0VI0JOVPHJJJW1JIJNX5HWCTP1DCMQ1RJFE22IFL0OX"
//    case clientSecret = "VBYPUXJL044KXSUY2RYYRAVY4DRKJR4XFL3AKMD5VB1EYSJQ"
//    case vValue = "20180405"
//    case nearValue = "New York"
//    case baseURL = "https://api.foursquare.com/v2/venues/explore?"
//
//    func getBaseURL() -> String {
//
//        return "https://api.foursquare.com/v2/venues/explore?client_id=THIZC0VI0JOVPHJJJW1JIJNX5HWCTP1DCMQ1RJFE22IFL0OX&client_secret=VBYPUXJL044KXSUY2RYYRAVY4DRKJR4XFL3AKMD5VB1EYSJQ&v=20180405&near=New York"
//    }
//}

class APIManager {
    func getGroups(completion: @escaping(_ : MerlinGroups?) -> Void) {
        var urlString = "https://api.foursquare.com/v2/venues/explore?client_id=THIZC0VI0JOVPHJJJW1JIJNX5HWCTP1DCMQ1RJFE22IFL0OX&client_secret=VBYPUXJL044KXSUY2RYYRAVY4DRKJR4XFL3AKMD5VB1EYSJQ&v=20180405&near=New York"
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        guard let url = URL(string: urlString) else {
            return
        }
        Alamofire.request(url).responseObject(keyPath: "response") { (response: DataResponse<MerlinGroups>) in
            guard let groups = response.result.value else {
                completion(nil)
                return
            }
            completion(groups)
        }

    }
}
